using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class MeshData {
  public List<Vector3> vertices = new List<Vector3>();
  public List<Vector3> normals = new List<Vector3>();
  public List<Color> colors = new List<Color>();
  public List<Vector3> uvs = new List<Vector3>();
  public List<int> triangles = new List<int>();

  public void Clear() {
    vertices.Clear();
    normals.Clear();
    colors.Clear();
    triangles.Clear();
  }

  public void AddTriangle(IEnumerable<Vector3> points, Vector3 normal, Color color) {
    AddTriangle(points, Enumerable.Repeat(normal, 3), Enumerable.Repeat(color, 3), null);
  }

  public void AddTriangle(IEnumerable<Vector3> points, IEnumerable<Vector3> normals,
                          IEnumerable<Color> colors, IEnumerable<Vector3> uvs = null) {
    int num = this.vertices.Count;
    this.vertices.AddRange(points);
    this.normals.AddRange(normals);
    this.colors.AddRange(colors);
    if (uvs != null) {
      this.uvs.AddRange(uvs);
    }
    triangles.AddRange(new int[] { num, num+1, num+2 });
  }

  public void AddQuad(Vector3[] points, Vector3 normal, Color color) {
    AddQuad(points, Enumerable.Repeat(normal, 3), Enumerable.Repeat(color, 3), null);
  }

  public void AddQuad(IEnumerable<Vector3> points, IEnumerable<Vector3> normals,
                      IEnumerable<Color> colors, IEnumerable<Vector3> uvs = null) {
    int num = this.vertices.Count;
    this.vertices.AddRange(points);
    this.normals.AddRange(normals);
    this.colors.AddRange(colors);
    if (uvs != null) {
      this.uvs.AddRange(uvs);
    }
    triangles.AddRange(new int[] { num, num+1, num+2, num, num+2, num+3 });
  }

  public void AddQuadIndices(int a, int b, int c, int d) {
    triangles.AddRange(new int[] { a, b, c, a, c, d });
  }

  public void AddTriangleIndices(int a, int b, int c) {
    triangles.AddRange(new int[] { a, b, c });
  }

  public MeshData Clone() {
    MeshData md = new MeshData();
    md.vertices = vertices.Clone();
    md.normals = normals.Clone();
    md.colors = colors.Clone();
    md.uvs = uvs.Clone();
    md.triangles = triangles.Clone();
    return md;
  }

  public void CreateMesh(Mesh mesh) {
    mesh.Clear();
    mesh.vertices = vertices.ToArray();
    mesh.colors = colors.ToArray();
    if (uvs.Count > 0) {
      mesh.SetUVs(0, uvs);
    }
    mesh.triangles = triangles.ToArray();
    if (normals.Count > 0) {
      mesh.normals = normals.ToArray();
    } else {
      mesh.RecalculateNormals();
    }
  }
}

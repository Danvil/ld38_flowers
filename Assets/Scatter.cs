﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Scatter : MonoBehaviour {

  public enum ScatterMode {
    Box,
    Sphere,
    SphereBoundary
  }

  public GameObject prefab;
  public int count = 25;
  public int countMax = 25;
  public float rate = 0f;
  public float range = 60f;
  public float scaleRange1 = 2f;
  public float scaleRange2 = 8f;
  public ScatterMode mode = ScatterMode.Box;
  public bool alignToCenter = false;
  public string groupName = "scatter";

  GameObject group;

  void Awake() {
    group = gameObject.EmptyGroup(groupName);
  }

  void Start() {
		for (int i=0; i<count; i++) {
      CreateOne();
    }
	}

  void CreateOne() {
    GameObject go = Instantiate(prefab);
    go.transform.parent = group.transform;
    go.transform.position = RandomPosition();
    go.transform.localScale = Noise.Instance.RandomFloat(scaleRange1, scaleRange2) * Vector3.one;
    if (alignToCenter) {
      go.transform.rotation = Quaternion.FromToRotation(Vector3.up, -go.transform.position.normalized);
    }
  }

  Vector3 RandomPosition() {
    switch(mode) {
      default: case ScatterMode.Box: return range * Noise.Instance.RandomVector3MP();
      case ScatterMode.Sphere: return range * Noise.Instance.RandomInSphere();
      case ScatterMode.SphereBoundary: return range * Noise.Instance.RandomOnSphere();
    }
  }
	
	void Update() {
		if (rate == 0f || group.transform.childCount >= countMax) {
      return;
    }
    if (Noise.Instance.RandomEvent(rate, Time.deltaTime)) {
      CreateOne();
    }
  }
}

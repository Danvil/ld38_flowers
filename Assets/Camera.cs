﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// dragging x/y -> rotate around poi
public class Camera : MonoBehaviour {

  public Transform poi;
  public float zoomPower = 1.4f;
  public float rotateRate = 0.9f;

  Vector3 last = new Vector3();
  float zoom = 10f;

  void Start() {
  }

  void Update() {
    Vector3 current = Input.mousePosition;
    Vector3 mouseDelta = current - last;
    last = current;
    if (Input.GetMouseButton(0)) {
      this.transform.RotateAround(poi.transform.position, this.transform.rotation * new Vector3(1,0,0), -rotateRate*mouseDelta.y);
      this.transform.RotateAround(poi.transform.position, this.transform.rotation * new Vector3(0,1,0), rotateRate * mouseDelta.x);
      //Quaternion qy = Quaternion.AngleAxis(mouseDelta.y, this.transform.rotation * new Vector3(1, 0, 0));
      //Quaternion qx = Quaternion.AngleAxis(mouseDelta.x, this.transform.rotation * new Vector3(0, 1, 0));
      //this.transform.position += qx * qy * (this.transform.position - poi.transform.position) - this.transform.position;
      //this.transform.rotation *= qx * qy;
      //this.transform.position += qx * (this.transform.position - poi.transform.position) - this.transform.position;
      //this.transform.rotation *= qx;
    }
    zoom *= Mathf.Pow(zoomPower, Input.GetAxis("Mouse ScrollWheel"));
    this.transform.position = poi.transform.position + zoom * (this.transform.position - poi.transform.position).normalized;
    this.transform.LookAt(poi);
  }
}

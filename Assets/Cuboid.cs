﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Specimen))]
public class Cuboid : MonoBehaviour {

  public Material materialPrefab;

  Specimen specimen;
  const int kGSize = 0;
  const int kGColorR = 1;
  const int kGColorG = 2;
  const int kGColorB = 3;
  const int kGProcreateProb = 4;
  const int kGLength = 5;

  public float Size { get { return specimen.genome[kGSize]; } }
  public Color Color { get { return new Color(specimen.genome[kGColorR], specimen.genome[kGColorG], specimen.genome[kGColorB]); } }

  void Mutate() {
    specimen = GetComponent<Specimen>();
    specimen.genome[kGSize] *= Mathf.Pow(1.2f, Noise.Instance.RandomFloat(-1, +1));
    specimen.genome[kGColorR] = Mathf.Clamp01(specimen.genome[kGColorR] + Noise.Instance.RandomFloatMP(0.1f));
    specimen.genome[kGColorG] = Mathf.Clamp01(specimen.genome[kGColorG] + Noise.Instance.RandomFloatMP(0.1f));
    specimen.genome[kGColorB] = Mathf.Clamp01(specimen.genome[kGColorB] + Noise.Instance.RandomFloatMP(0.1f));
    specimen.genome[kGProcreateProb] *= Mathf.Pow(1.1f, Noise.Instance.RandomFloat(-1, +1));
  }

  void InitializeGenome() {
    specimen = GetComponent<Specimen>();
    specimen.genome = new float[kGLength];
    specimen.genome[kGSize] = 3.0f;
    specimen.genome[kGColorR] = 0.8f;
    specimen.genome[kGColorG] = 0.8f;
    specimen.genome[kGColorB] = 0.8f;
    specimen.genome[kGProcreateProb] = 0.1f;
    Mutate();
  }

  void OnProcreate(Specimen spec) {
    spec.GetComponent<Cuboid>().Mutate();
    spec.transform.position += Noise.Instance.RandomVector3MP(2.0f * Size);
  }

  Material material;
  void SetColor(Color color) {
    material.SetColor("_Color", color);
  }

  void RealizePhenotype() {
    specimen.growthRate = 1.0f / Size;
    specimen.procreationRate = specimen.genome[kGProcreateProb];
    specimen.maxAge *= Mathf.Pow(1.5f, Noise.Instance.RandomFloat(-1, +1));
    specimen.radius = 0.5f * Size;
    material = Instantiate(materialPrefab);
    SetColor(Color);
    GetComponent<Renderer>().material = material;
  }

  void Awake() {
    specimen = GetComponent<Specimen>();
    specimen.onProcreate = OnProcreate;
    if (specimen.genome.Length == 0) {
      InitializeGenome();
    }
  }

  void Start() {
    this.transform.localScale = Vector3.zero;
    RealizePhenotype();
	}

	void Update() {
    this.transform.localScale = specimen.Growth * Size * Vector3.one;
    SetColor(Color.Lerp(Color, Color.black, specimen.Decay));
	}
}

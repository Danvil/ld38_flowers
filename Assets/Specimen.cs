﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Specimen : MonoBehaviour {

  public float[] genome;
  public float growthRate = 0.1f;
  public float decayRate = 0.3f;
  public float maxAge = 20f;
  public float procreationRate = 0.2f;
  public float radius = 0.5f;

  public Garden garden;

  float growth;
  float age;
  float decay;

  public float Growth { get { return growth; } }
  public float Age { get { return age; } }
  public float Decay { get { return decay; } }
  public bool IsDead { get { return decay >= 1f; } }

  public Action<Specimen> onProcreate;

  public float effectiveProcreationRate = 0.2f;

  public Specimen Procreate() {
    if (Noise.Instance.RandomEvent(effectiveProcreationRate, Time.deltaTime)) {
      Specimen spec = Instantiate(gameObject).GetComponent<Specimen>();
      if (onProcreate != null) {
        onProcreate(spec);
      }
      return spec;
    } else {
      return null;
    }
  }

  void Start () {
    growth = 0f;
    age = 0f;
    decay = 0f;
	}
	
	void Update () {
    growth = Mathf.Min(1f, growth + growthRate * Time.deltaTime);
    age += Time.deltaTime;
    if (age > maxAge) {
      decay = Mathf.Min(1f, decay + Time.deltaTime);
    }
	}
}

﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;

public static class GameObjectExtensions {
  // Creates an empty child game object with given name (deletes first if already exists)
  public static GameObject EmptyGroup(this GameObject go, string name) {
    Transform childTransform = go.transform.Find(name);
    if (childTransform != null) {
      GameObject.DestroyImmediate(childTransform.gameObject);
    }
    GameObject child = new GameObject();
    child.name = name;
    child.transform.parent = go.transform;
    child.transform.localPosition = Vector3.zero;
    return child;
  }

  public static List<Vector3> Clone(this List<Vector3> values) {
    return values.Select(x => x).ToList();
  }
  public static List<Color> Clone(this List<Color> values) {
    return values.Select(x => x).ToList();
  }
  public static List<int> Clone(this List<int> values) {
    return values.Select(x => x).ToList();
  }

  public static void Quer(Vector3 n, out Vector3 u, out Vector3 v) {
    Vector3 u1 = n + Vector3.up;
    Vector3 u2 = n + Vector3.right;
    Vector3 u3 = n + Vector3.forward;
    float q1 = Mathf.Abs(Vector3.Dot(n, u1));
    float q2 = Mathf.Abs(Vector3.Dot(n, u2));
    float q3 = Mathf.Abs(Vector3.Dot(n, u3));
    if (q1 < q2 && q1 < q3) {
      u = Vector3.Cross(u1, n);
    } else if (q2 < q3) {
      u = Vector3.Cross(u2, n);
    } else {
      u = Vector3.Cross(u3, n);
    }
    u.Normalize();
    v = Vector3.Cross(u, n);
  }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class SphereCreator : MonoBehaviour {

  Sphere[] spheres;

  void Awake() {
  }

  void Start() {
    spheres = GetComponentsInChildren<Sphere>();
    foreach(Sphere sphere in spheres) {
      sphere.sphereness = Noise.Instance.RandomFloat();
      sphere.radius = 0.5f; // Noise.Instance.RandomFloat(0.4f, 0.6f);
      sphere.Create();
    }
  }

  void Update() {

  }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SproutTester : MonoBehaviour {

  public bool isColliding = false;

  void OnCollisionEnter(Collision collision) {
    isColliding = true;
  }

}

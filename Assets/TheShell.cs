﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class TheShell : MonoBehaviour {

  public static TheShell Instance;

  public float radius = 40f;
  public float splashRadius = 6f;
  public int splashNumParticles = 10;
  public WaterParticle waterParticlePrefab;

  GameObject splashes;
  List<WaterParticle> waterParticles = new List<WaterParticle>();

  float Humidity(float distance) {
    return Mathf.Max(0f, 1f - distance / splashRadius);
  }

  public float GetHumidity(Vector3 position) {
    if (waterParticles.Count == 0) {
      return 0f;
    }
    return waterParticles.Max(wp => Humidity((wp.transform.position - position).magnitude));
  }

  void Awake() {
    Instance = this;
    splashes = gameObject.EmptyGroup("splashes");
  }

	void Start() {
		
	}
	
	void Update() {
    WaterParticle[] exhausted = waterParticles.Where(wp => wp.IsExhausted).ToArray();
    foreach(var wp in exhausted) {
      Destroy(wp.gameObject);
      waterParticles.Remove(wp);
    }
	}

  public void SplashWater(Vector3 position) {
    position = radius * position.normalized;
    for (int i=0; i<splashNumParticles; i++) {
      WaterParticle wp = Instantiate(waterParticlePrefab);
      wp.transform.parent = splashes.transform;
      Vector3 delta = Noise.Instance.RandomInSphere();
      wp.transform.position = (radius - 1f) * (position + splashRadius * delta).normalized;
      waterParticles.Add(wp);
    }
  }
}

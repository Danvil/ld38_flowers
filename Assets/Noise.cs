﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Noise : MonoBehaviour {

  public static Noise Instance;
  public int seed = 1337;

  System.Random random;

  public void Awake() {
    Instance = this;
    random = new System.Random(seed);
  }
  // Pick a random element from a list
  public T RandomElement<T>(List<T> list) {
    return list[RandomInt(list.Count)];
  }
  // Pick a random element from an array
  public T RandomElement<T>(T[] array) {
    return array[RandomInt(array.Length)];
  }
  // A random integer in the inteval [0|n[
  public int RandomInt(int max) {
    return random.Next(max);
  }
  // Random 32-bit float in [0|1]
  public float RandomFloat() {
    return (float)random.NextDouble();
  }
  // Random 32-bit float in [a|b]
  public float RandomFloat(float a, float b) {
    return a + (b - a) * RandomFloat();
  }
  // Random 32-bit float in [0|a]
  public float RandomFloat(float a) {
    return a * RandomFloat();
  }
  // Random 32-bit float in [-1|+1]
  public float RandomFloatMP() {
    return -1f + 2f * RandomFloat();
  }
  // Random 32-bit float in [-a|+a]
  public float RandomFloatMP(float a) {
    return a * RandomFloatMP();
  }
  // 3D-vector of RandomFloatMP
  public Vector3 RandomVector3MP() {
    return new Vector3(RandomFloatMP(), RandomFloatMP(), RandomFloatMP());
  }
  public Vector3 RandomVector3MP(float a) {
    return a * new Vector3(RandomFloatMP(), RandomFloatMP(), RandomFloatMP());
  }
  // A normal distributed 2D vector with given standard deviation
  public Vector2 RandomGaussian2(float sigma) {
    // http://mathworld.wolfram.com/Box-MullerTransformation.html
    float a = RandomFloat(2f * Mathf.PI);
    float b = sigma * Mathf.Sqrt(-2.0f * Mathf.Log(RandomFloat()));
    return new Vector2(b * Mathf.Cos(a), b * Mathf.Sin(a));
  }
  public float RandomGaussian(float sigma) {
    return RandomGaussian2(sigma).x;
  }
  // A random position inside a circle of given radius
  public Vector2 RandomInDisk(float radius) {
    float r = RandomFloat(radius);
    float a = RandomAngleRad();
    return new Vector2(r * Mathf.Cos(a), r * Mathf.Sin(a));
  }
  // A random rotation angle in radians
  public float RandomAngleRad() {
    return RandomFloat(2f * Mathf.PI);
  }
  // A random point on a unit sphere
  public Vector3 RandomOnSphere() {
    while (true) {
      float x0 = RandomFloatMP();
      float x1 = RandomFloatMP();
      float x2 = RandomFloatMP();
      float x3 = RandomFloatMP();
      float x00 = x0 * x0;
      float x11 = x1 * x1;
      float x22 = x2 * x2;
      float x33 = x3 * x3;
      float check = x00 + x11 + x22 + x33;
      if (check >= 1f || check == 0f) {
        continue;
      }
      float x = 2f * (x1 * x3 + x0 * x2);
      float y = 2f * (x2 * x3 - x0 * x1);
      float z = x00 + x33 - x11 - x22;
      return new Vector3(x, y, z) / check;
    }
  }
  // A random point on a sphere with given radius
  public Vector3 RandomOnSphere(float radius) {
    return radius * RandomOnSphere();
  }
  // A random point in a sphere
  public Vector3 RandomInSphere() {
    return RandomOnSphere(Mathf.Pow(RandomFloat(), 0.33333f));
  }
  // A random rotation with most magnitude a in def
  public Quaternion RandomRotationDeg(float a_deg) {
    return Quaternion.AngleAxis(RandomFloat(a_deg), RandomOnSphere());
  }
  // Checks if an event of given probability occurred
  public bool RandomCoin(float probability) {
    return RandomFloat() < probability;
  }
  // Checks if an event of given rate occurred in time interval
  public bool RandomEvent(float rate, float dt) {
    return RandomCoin(1f - Mathf.Exp(-rate * dt));
  }
}

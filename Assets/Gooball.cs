﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gooball : MonoBehaviour {

  public float startRadius = 0f;
  public float finalRadius = 3f;
  public float radiusGrowth = 0.5f;
  public float velocityDecayRate = 0.2f;

  Floaty floaty;
  float radius;
  Vector3 velocity;

  void Awake() {
    floaty = this.GetComponentInChildren<Floaty>();
  }

  void Start() {
    radius = startRadius;
    this.transform.localScale = radius * Vector3.one;
    velocity = Vector3.zero;
  }
	
	void Update() {
    radius = Mathf.Clamp(radius + Time.deltaTime * radiusGrowth, 0f, finalRadius);
    this.transform.localScale = radius * Vector3.one;
    this.transform.position += Time.deltaTime * velocity;
    velocity *= Mathf.Exp(-velocityDecayRate * Time.deltaTime);
    floaty.enabled = false; //(velocity.magnitude < 0.05f);
    if (this.transform.position.magnitude + radius >= TheShell.Instance.radius) {
      Explode();
    }
  }

  public void Tap(Vector3 point, float strength) {
    Vector3 delta = this.transform.position - point;
    velocity += strength * delta.normalized;
  }

  //void OnCollisionExit(Collision collision) {
  //  Debug.Log("COLLISION " + collision.gameObject.name);
  //  if (collision.gameObject == TheShell.Instance.gameObject) {
  //    Explode();
  //  }
  //}

  void Explode() {
    TheShell.Instance.SplashWater(this.transform.position);
    Destroy(gameObject);
  }
}

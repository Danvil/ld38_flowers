﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[ExecuteInEditMode]
public class Sphere : MonoBehaviour {

  public int samples = 100;
  public float radius = 10f;
  public bool normalsInside = false;
  public float sphereness = 0.8f;

  MeshFilter meshFilter;

  void Awake() {
    meshFilter = GetComponent<MeshFilter>();
  }

  List<Vector3> vertices;
  List<int[]> neighbours;

  struct Triangle {
    public Triangle(int na, int nb, int nc) { a = na; b = nb; c = nc; }
    public int a, b, c;
  }

  void CreateBlah() {
    // seed
    List<Vector3> vertices = new List<Vector3>();
    vertices.Add(new Vector3(0f, +1f, 0f));
    vertices.Add(new Vector3(+1f, 0f, 0f));
    vertices.Add(new Vector3(0f, 0f, +1f));
    vertices.Add(new Vector3(-1f, 0f, 0f));
    vertices.Add(new Vector3(0f, 0f, -1f));
    vertices.Add(new Vector3(0f, -1f, 0f));
    List<Triangle> triangles = new List<Triangle>();
    triangles.Add(new Triangle(1, 0, 2));
    triangles.Add(new Triangle(2, 0, 3));
    triangles.Add(new Triangle(3, 0, 4));
    triangles.Add(new Triangle(4, 0, 1));
    triangles.Add(new Triangle(5, 1, 2));
    triangles.Add(new Triangle(5, 2, 3));
    triangles.Add(new Triangle(5, 3, 4));
    triangles.Add(new Triangle(5, 4, 1));
    // subdivide
    for (int i = 0; i < 3; i++) {
      List<Triangle> trianglesOld = triangles;
      triangles = new List<Triangle>();
      triangles.Capacity = trianglesOld.Count * 3;
      foreach (Triangle triangle in trianglesOld) {
        // sample point in middle
        Vector3 va = vertices[triangle.a];
        Vector3 vb = vertices[triangle.b];
        Vector3 vc = vertices[triangle.c];
        Vector3 p = 0.333f * (va + vb + vc);
        Vector3 delta = va - p;
        float r = 0.0f * delta.magnitude;
        Vector3 normal = p.normalized;
        Vector3 tangent1 = delta.normalized;
        Vector3 tangent2 = Vector3.Cross(tangent1, normal);
        Vector2 deviation = Noise.Instance.RandomInDisk(r);
        Vector3 newp = p + deviation.x * tangent1 + deviation.y * tangent2;
        int newi = vertices.Count;
        vertices.Add(newp.normalized);
        triangles.Add(new Triangle(triangle.a, newi, triangle.b));
        triangles.Add(new Triangle(triangle.b, newi, triangle.c));
        triangles.Add(new Triangle(triangle.c, newi, triangle.a));
      }
    }
    // create mesh
    Mesh mesh = new Mesh();
    mesh.vertices = vertices.ToArray();
    int[] vertexArray = new int[triangles.Count * 3];
    for(int i=0; i<triangles.Count; i++) {
      vertexArray[3 * i] = triangles[i].a;
      vertexArray[3 * i + 1] = triangles[i].b;
      vertexArray[3 * i + 2] = triangles[i].c;
    }
    mesh.triangles = vertexArray;
    mesh.RecalculateNormals();
    meshFilter.mesh = mesh;
  }

  Vector3[] CubeToSphereOrigins = {
    new Vector3(-1f, -1f, -1f),
    new Vector3(1f, -1f, -1f),
    new Vector3(1f, -1f, 1f),
    new Vector3(-1f, -1f, 1f),
    new Vector3(-1f, 1f, -1f),
    new Vector3(-1f, -1f, 1f)
  };
  Vector3[] CubeToSphereRights = {
    new Vector3(2f, 0f, 0f),
    new Vector3(0f, 0f, 2f),
    new Vector3(-2f, 0f, 0f),
    new Vector3(0f, 0f, -2f),
    new Vector3(2f, 0f, 0f),
    new Vector3(2f, 0f, 0f)
  };
  Vector3[] CubeToSphereUps = {
    new Vector3(0f, 2f, 0f),
    new Vector3(0f, 2f, 0f),
    new Vector3(0f, 2f, 0f),
    new Vector3(0f, 2f, 0f),
    new Vector3(0f, 0f, 2f),
    new Vector3(0f, 0f, -2f)
  };

  public int divisions = 2;

  public void Create() {
    List<Vector3> vertices = new List<Vector3>();
    List<int> triangles = new List<int>();

    float step = 1f / (float)divisions;

    for (int face = 0; face < 6; ++face) {
      Vector3 origin = CubeToSphereOrigins[face];
      Vector3 right = CubeToSphereRights[face];
      Vector3 up = CubeToSphereUps[face];
      for (int j = 0; j < divisions + 1; ++j) {
        for (int i = 0; i < divisions + 1; ++i) {
          Vector3 p = origin + step * (((float)i) * right + ((float)j) * up);
          Vector3 p2 = new Vector3(origin.x*origin.x, origin.y*origin.y, origin.z*origin.z);
           Vector3 n = new Vector3(
            p.x * Mathf.Sqrt(1f - 0.5f * (p2.y + p2.z) + p2.y*p2.z / 3f),
            p.y * Mathf.Sqrt(1f - 0.5f * (p2.z + p2.x) + p2.z*p2.x / 3f),
            p.z * Mathf.Sqrt(1f - 0.5f * (p2.x + p2.y) + p2.x*p2.y / 3f)
          );
          vertices.Add(radius * (sphereness * n.normalized + (1f - sphereness) * n));
        }
      }
    }

    int k = divisions + 1;
    for (int face = 0; face < 6; ++face) {
      for (int j = 0; j < divisions; ++j) {
        bool bottom = j < (divisions / 2);
        for (int i = 0; i < divisions; ++i) {
          bool left = i < (divisions / 2);
          int a = (face * k + j) * k + i;
          int b = (face * k + j) * k + i + 1;
          int c = (face * k + j + 1) * k + i;
          int d = (face * k + j + 1) * k + i + 1;
          if (bottom != left) {
            if (normalsInside) {
              triangles.AddRange(new int[] { a, d, c, a, b, d });
            } else {
              triangles.AddRange(new int[] { a, c, d, a, d, b });
            }
          }
          else {
            if (normalsInside) {
              triangles.AddRange(new int[] { a, b, c, c, b, d });
            } else {
              triangles.AddRange(new int[] { a, c, b, c, d, b });
            }
          }
        }
      }
    }

    Mesh mesh = new Mesh();
    mesh.vertices = vertices.ToArray();
    mesh.triangles = triangles.ToArray();
    mesh.RecalculateNormals();
    meshFilter.mesh = mesh;
  }

  void Start() {
    Create();
  }

	void Update() {
	}

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[ExecuteInEditMode]
public class FlowerHead : MonoBehaviour {

  public float petalDistance = 0.4f;
  const int kNumWidth = 10;
  const int kNumLength = 10;
  public int petalCount = 4;
  public float petalWidthBase = 0.5f;
  public float petalWidthTip = 1.2f;
  public float petalLengthCenter = 1.5f;
  public float petalLengthRim = 1.2f;
  public float petalThicknessRim = 0f;
  public float petalThicknessCenter = 0.1f;
  public float petalBendBaseTip = 0f;
  public float petalBendBase = 0.25f;
  public float petalBendTip = -0.25f;
  public float petalBendCenter = -0.25f;
  public float petalBendRim = 0.25f;

  public float petalAngleNoise = 10f;
  public float petalOffsetNoise = 0.05f;

  MeshFilter meshFilter;
  Mesh mesh;

  class Pose {
    public Quaternion rotation;
    public Vector3 translation;
    public Pose(Quaternion q, Vector3 p) {
      rotation = q;
      translation = p;
    }
    public Vector3 transform(Vector3 v) {
      return rotation * v + translation;
    }
  }

  void Awake() {
    meshFilter = GetComponent<MeshFilter>();
    mesh = new Mesh();
    meshFilter.mesh = mesh;
  }

  void Start() {
    Create();
  }

  void Update() {
  }

  static float Lerp(float p, float a, float b) {
    return (1f - p) * a + p * b;
  }

  float PedalBorder(float tx, float tz) {
    return 1f - 4f * tx * (tx - 1f) * (tz - 1f);
  }

  float PedalBend(float tx, float tz) {
    float bx = Lerp(tz, petalBendBase, petalBendTip);
    float bz = Lerp(Mathf.Abs(2f * tx - 1), petalBendCenter, petalBendRim);
    return bx * 4f * (tx-0.5f) * (tx-0.5f) + bz * tz * tz + petalBendBaseTip * tz;
  }

  Vector3 FlowerPedalEquation(float tx, float tz, bool front) {
    float border = PedalBorder(tx, tz);
    float bend = PedalBend(tx, tz);
    tx -= 0.5f;
    float width = Lerp(tz, petalWidthBase, petalWidthTip);
    float len = Lerp(Mathf.Abs(4f * tx * tx), petalLengthCenter, petalLengthRim);
    float hoffset = Lerp(border, petalThicknessCenter, petalThicknessRim);
    float h = bend + (front ? +1f : -1f) * 0.5f * hoffset;
    return new Vector3(tx * width, h, tz * len);
  }

  void CreateFlowerPedalSide(MeshData md, Pose pose, bool front) {
    Color color = new Color(1f, 1f, 1f);
    int index = md.vertices.Count;
    for (int i = 0; i <= kNumWidth; i++) {
      for (int j = 0; j <= kNumLength; j++) {
        float tx = i / (float)kNumWidth;
        float tz = j / (float)kNumLength;
        md.vertices.Add(pose.transform(FlowerPedalEquation(tx, tz, front)));
        md.colors.Add(color);
      }
    }
    for (int i = 0; i < kNumWidth; i++) {
      for (int j = 0; j < kNumLength; j++) {
        int a = index + i * (kNumLength + 1) + j;
        if (front) {
          md.AddQuadIndices(a, a + 1, a + 2 + kNumLength, a + kNumLength + 1);
        } else {
          md.AddQuadIndices(a, a + kNumLength + 1, a + 2 + kNumLength, a + 1);
        }
      }
    }
  }

  void CreatePedal(MeshData md, Pose pose) {
    CreateFlowerPedalSide(md, pose, false);
    CreateFlowerPedalSide(md, pose, true);
  }

  void Create() {
    MeshData md = new MeshData();
    // create pedals
    for (int k = 0; k < petalCount; k++) {
      float angle = (float)k / (float)petalCount * 360f + 0f*Noise.Instance.RandomFloatMP(petalAngleNoise);
      float offset = petalDistance + 0f * Noise.Instance.RandomFloatMP(petalOffsetNoise);
      Quaternion q = Quaternion.AngleAxis(angle, Vector3.up);
      Pose pose = new Pose(q * Quaternion.AngleAxis(-90f, Vector3.right), q * new Vector3(0f, 0f, offset));
      CreatePedal(md, pose);
    }
    // set mesh
    md.CreateMesh(mesh);
  }
}

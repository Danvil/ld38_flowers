﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
public class Sprout : MonoBehaviour {

  public int segments = 12;
  public float segmentRadius = 0.1f;
  public float segmentRotation = 20f;
  public float segmentLength = 1.0f;
  public int segmentSectors = 8;
  public float growthSpeed = 1f;
  public float minGrowthForUpdate = 0.1f;

  public SproutTester testerPrefab;
  SproutTester tester;

  MeshFilter meshFilter;
  Mesh mesh;
  MeshData meshData, growingMeshData;

  Vector3 currentPosition;
  Vector3 currentTargetPosition;
  Quaternion currentRotation;
  Quaternion currentTargetRotation;
  int steps;
  float growth;
  float lastGrowth;

  enum GrowthStage { Inactive, Trying0, Trying, Growing };
  GrowthStage stage;
  int tryingCount;

  GameObject testers;

  public bool IsGrown { get { return steps == segments; } }

  void Awake() {
    testers = gameObject.EmptyGroup("testers");
    meshFilter = GetComponent<MeshFilter>();
    mesh = new Mesh();
    meshFilter.mesh = mesh;
  }

  void Start() {
    Reset();
  }

  void Update() {
    if (stage == GrowthStage.Inactive) {
      if (steps < segments) {
        tester = Instantiate(testerPrefab);
        tester.transform.SetParent(testers.transform, true);
        tester.transform.localPosition = tester.transform.localPosition;
        tester.transform.localRotation = tester.transform.localRotation;
        tester.transform.localScale = Vector3.one;
        tester.name = string.Format("tester {0}", steps);
        stage = GrowthStage.Trying0;
        tryingCount = 0;
      }
    } else if (stage == GrowthStage.Trying0) {
      TryGrow();
      stage = GrowthStage.Trying;
    } else if (stage == GrowthStage.Trying) {
      tryingCount++;
      if (tryingCount > 1) {
        if (tester.isColliding) {
          stage = GrowthStage.Trying0;
        } else {
          growth = 0f;
          lastGrowth = 0f;
          stage = GrowthStage.Growing;
        }
      }
    } else if (stage == GrowthStage.Growing) {
      growth += growthSpeed * Time.deltaTime;
      if (growth >= 1f) {
        growth = 1f;
        Grow();
        steps++;
        stage = GrowthStage.Inactive;
        meshData = growingMeshData;
        currentPosition = tester.transform.localPosition;
        currentRotation = tester.transform.localRotation;
      } else {
        if (growth > lastGrowth + minGrowthForUpdate) {
          lastGrowth = growth;
          Grow();
        }
      }
    }
  }

  void Reset() {
    meshData = new MeshData();
    meshData.CreateMesh(mesh);
    currentPosition = Vector3.zero;
    currentTargetPosition = currentPosition;
    currentRotation = Quaternion.identity;
    currentTargetRotation = currentRotation;
    stage = GrowthStage.Inactive;
    steps = 0;
    CreateVertexRing(meshData);
  }

  void TryGrow() {
    tester.transform.localPosition = currentPosition + segmentLength * (currentRotation * Vector3.up);
    tester.transform.localRotation = currentRotation * Noise.Instance.RandomRotationDeg(segmentRotation);
  }

  void CreateVertexRing(MeshData md) {
    Quaternion rot = Quaternion.AngleAxis(360f / segmentSectors, Vector3.up);
    Vector3 p = new Vector3(segmentRadius, 0, 0);
    for (int i = 0; i < segmentSectors; i++) {
      md.vertices.Add(currentTargetPosition + currentTargetRotation * p);
      p = rot * p;
    }
  }

  void CreateCap(MeshData md, int baseindex) {
    for (int i = 1; i < segmentSectors; i++) {
      int ip = (i + 1) % segmentSectors;
      growingMeshData.AddTriangleIndices(baseindex, baseindex + i, baseindex + ip);
    }
  }

  void Grow() {
    growingMeshData = meshData.Clone();
    // create mesh vertices
    int baseindex = growingMeshData.vertices.Count;
    currentTargetPosition = (1f - growth) * currentPosition + growth * tester.transform.localPosition;
    currentTargetRotation = Quaternion.Lerp(currentRotation, tester.transform.localRotation, growth);
    CreateVertexRing(growingMeshData);
    // create mesh triangles
    for (int i=0; i<segmentSectors; i++) {
      int index1 = baseindex;
      int index2 = baseindex - segmentSectors;
      int ip = (i + 1) % segmentSectors;
      growingMeshData.AddQuadIndices(index1 + i, index2 + i, index2 + ip, index1 + ip);
    }
    CreateCap(meshData, baseindex);
    // realize
    growingMeshData.CreateMesh(mesh);
  }

  public void SetToTip(Transform transform) {
    transform.localRotation = currentTargetRotation;
    transform.localPosition = currentTargetPosition;
  }
}

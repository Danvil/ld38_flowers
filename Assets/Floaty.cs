﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floaty : MonoBehaviour {

  [Range(0, 10)]
  public float rate = 0.5f;
  [Range(0, 100)]
  public float amplitude = 2f;
  [Range(0, 10)]
  public float drift = 0.5f;

  Vector3 pole;
  Vector3 velocity;

	void Start() {
    pole = amplitude * Noise.Instance.RandomOnSphere();
    velocity = pole;
  }

  void Update() {
    float dt = Time.deltaTime;
    pole += dt * drift * Noise.Instance.RandomVector3MP();
    pole = amplitude * pole.normalized;
    Vector3 target = pole;
    if (Vector3.Dot(pole, this.transform.localPosition) > 0) {
      target = -target;
    }
    Vector3 delta = target - this.transform.localPosition;
    velocity += dt * rate * delta;
    this.transform.localPosition += dt * velocity;
	}
}

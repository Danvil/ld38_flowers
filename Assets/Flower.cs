﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Specimen))]
public class Flower : MonoBehaviour {

  const int kGSegments = 0;
  const int kGTrunkColorR = 1;
  const int kGTrunkColorG = 2;
  const int kGTrunkColorB = 3;
  const int kGPetalColorR = 4;
  const int kGPetalColorG = 5;
  const int kGPetalColorB = 6;
  const int kGProcreateProb = 7;
  const int kGHeadSize = 8;
  const int kGLength = 9;

  public Sprout sprout;
  public FlowerHead head;

  Specimen specimen;

  public Color TrunkColor { get {
    return new Color(specimen.genome[kGTrunkColorR],
                     specimen.genome[kGTrunkColorG],
                     specimen.genome[kGTrunkColorB]); } }
  public Color PetalColor { get {
    return new Color(specimen.genome[kGPetalColorR],
                     specimen.genome[kGPetalColorG],
                     specimen.genome[kGPetalColorB]); } }
  public float HeadScale { get { return specimen.genome[kGHeadSize]; } }

  void Mutate() {
    specimen = GetComponent<Specimen>();
    specimen.genome[kGSegments] *= Mathf.Pow(1.2f, Noise.Instance.RandomFloat(-1, +1));
    specimen.genome[kGTrunkColorR] = Mathf.Clamp01(specimen.genome[kGTrunkColorR] + Noise.Instance.RandomFloatMP(0.1f));
    specimen.genome[kGTrunkColorG] = Mathf.Clamp01(specimen.genome[kGTrunkColorG] + Noise.Instance.RandomFloatMP(0.1f));
    specimen.genome[kGTrunkColorB] = Mathf.Clamp01(specimen.genome[kGTrunkColorB] + Noise.Instance.RandomFloatMP(0.1f));
    specimen.genome[kGPetalColorR] = Mathf.Clamp01(specimen.genome[kGPetalColorR] + Noise.Instance.RandomFloatMP(0.1f));
    specimen.genome[kGPetalColorG] = Mathf.Clamp01(specimen.genome[kGPetalColorG] + Noise.Instance.RandomFloatMP(0.1f));
    specimen.genome[kGPetalColorB] = Mathf.Clamp01(specimen.genome[kGPetalColorB] + Noise.Instance.RandomFloatMP(0.1f));
    specimen.genome[kGProcreateProb] *= Mathf.Pow(1.1f, Noise.Instance.RandomFloat(-1, +1));
    specimen.genome[kGHeadSize] *= Mathf.Pow(1.2f, Noise.Instance.RandomFloat(-1, +1));
  }

  static float[] hues = new float[] { 0f, 25f/360f, 54f/360f, 202f/360f, 299f/360f };

  float RandomFlowerColorHue() {
    return Noise.Instance.RandomElement(hues);
  }

  void InitializeGenome() {
    specimen = GetComponent<Specimen>();
    specimen.genome = new float[kGLength];
    specimen.genome[kGSegments] = 8.0f;
    specimen.genome[kGTrunkColorR] = 25f / 255f;
    specimen.genome[kGTrunkColorG] = 118f / 255f;
    specimen.genome[kGTrunkColorB] = 0f;
    Color petalcolor = Color.HSVToRGB(RandomFlowerColorHue(), 0.95f, 0.95f);
    specimen.genome[kGPetalColorR] = petalcolor.r;
    specimen.genome[kGPetalColorG] = petalcolor.g;
    specimen.genome[kGPetalColorB] = petalcolor.b;
    specimen.genome[kGProcreateProb] = 0.1f;
    specimen.genome[kGHeadSize] = 3.4f;
    Mutate();
  }

  void RealizePhenotype() {
    sprout.segments = 1 + Mathf.FloorToInt(specimen.genome[kGSegments]);
    specimen.radius = 1.5f;
    specimen.growthRate = 0.1f;
    sprout.growthSpeed = sprout.segments * specimen.growthRate;
    specimen.procreationRate = specimen.genome[kGProcreateProb];
    specimen.maxAge *= Mathf.Pow(1.1f, Noise.Instance.RandomFloat(-1, +1));
    trunkMaterial = Instantiate(sprout.GetComponent<Renderer>().material);
    sprout.GetComponent<Renderer>().material = trunkMaterial;
    petalMaterial = Instantiate(head.GetComponent<Renderer>().material);
    head.GetComponent<Renderer>().material = petalMaterial;
  }

  void OnProcreate(Specimen spec) {
    spec.GetComponent<Flower>().Mutate();
    spec.transform.position += Noise.Instance.RandomVector3MP(3f + specimen.radius);
  }

  Material trunkMaterial;
  void SetTrunkColor(Color color) {
    trunkMaterial.SetColor("_Color", color);
  }

  Material petalMaterial;
  void SetPetalColor(Color color) {
    petalMaterial.SetColor("_Color", color);
  }

  void Awake() {
    specimen = GetComponent<Specimen>();
    specimen.onProcreate = OnProcreate;
    if (specimen.genome.Length == 0) {
      InitializeGenome();
    }
  }

	void Start() {
    head.gameObject.SetActive(true);
    head.transform.localScale = Vector3.zero;
    RealizePhenotype();
  }

  void Update() {
    sprout.SetToTip(head.transform);
    head.transform.localScale = specimen.Growth * HeadScale * Vector3.one;
    SetTrunkColor(Color.Lerp(TrunkColor, Color.black, specimen.Decay));
    SetPetalColor(Color.Lerp(PetalColor, Color.black, specimen.Decay));
	}

}

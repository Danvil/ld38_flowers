﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterParticle : MonoBehaviour {

  public float strengthDecay = 0.2f;

  float strength;
  public bool IsExhausted { get { return strength <= 0f; } }

  void Start() {
    strength = 1f;
	}
	
	void Update() {
    strength = Mathf.Max(0f, strength - strengthDecay*Time.deltaTime);
	}
}

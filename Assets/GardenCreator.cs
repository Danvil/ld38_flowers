﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GardenCreator : MonoBehaviour {

  public Garden garden;

	void Start () {
    foreach(var specimen in GetComponentsInChildren<Specimen>()) {
      garden.Add(specimen);
    }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class Garden : MonoBehaviour {

  const float kMinDistance = 1.0f;
  public float humidityBase = 0.1f;
  public float humidityBoost = 5f;

  public int maxSpecimens = 100;

  List<Specimen> specimens = new List<Specimen>();

  public void Add(Specimen specimen) {
    specimens.Add(specimen);
  }

  public void Remove(Specimen specimen) {
    specimen.gameObject.SetActive(false);
    Destroy(specimen.gameObject);
    specimens.Remove(specimen);
  }

  void Start() {
    InvokeRepeating("UpdateEffectiveProcreation", 1.0f, 0.5f);
  }

  bool IsValidSeedLocation(Specimen spec) {
    spec.transform.position = 40.0f * spec.transform.position.normalized;
    return specimens.All(x => (x.transform.position - spec.transform.position).magnitude
                             > x.radius + spec.radius);
  }
	
	void Update() {
    foreach(Specimen spec in specimens) {
      if (spec.IsDead) Destroy(spec.gameObject);
    }
    specimens.RemoveAll(x => x.IsDead);
    if (specimens.Count < maxSpecimens) {
      Specimen[] seedlings = specimens.Select(x => x.Procreate()).Where(x => x != null).ToArray();
      foreach (Specimen spec in seedlings) {
        if (IsValidSeedLocation(spec)) {
          Add(spec);
        } else {
          Remove(spec);
        }
      }
    }
	}

  void UpdateEffectiveProcreation() {
    foreach(Specimen spec in specimens) {
      float boost = humidityBase + humidityBoost * TheShell.Instance.GetHumidity(spec.transform.position);
      spec.effectiveProcreationRate = spec.procreationRate * boost;
    }
  }
}

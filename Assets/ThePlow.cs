﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThePlow : MonoBehaviour {

  public new UnityEngine.Camera camera;
  public Garden garden;

  public float strengthRate = 3.0f;
  public float strengthMax = 10f;
  public float tapMouseMoveTolerance = 10f;

  float strength;
  Vector3 mouseWhenDown;

	void Start() {
    strength = 0f;
	}
	
	void Update() {
    if (Input.GetMouseButton(1)) {
      RaycastHit hitInfo = new RaycastHit();
      bool hit = Physics.Raycast(camera.ScreenPointToRay(Input.mousePosition), out hitInfo);
      if (hit) {
        Debug.Log("Hit " + hitInfo.transform.gameObject.name);
        Specimen spec = hitInfo.transform.gameObject.GetComponent<Specimen>();
        if (spec == null) {
          spec = hitInfo.transform.gameObject.GetComponentInParent<Specimen>();
        }
        if (spec != null) {
          garden.Remove(spec);
          Debug.Log("Removed specimen!");
        }
      }
    }
    if (Input.GetMouseButtonDown(0)) {
      strength = 0f;
      mouseWhenDown = Input.mousePosition;
    }
    if (Input.GetMouseButton(0)) {
      strength = Mathf.Min(strength + strengthRate * Time.deltaTime, strengthMax);
    }
    if (Input.GetMouseButtonUp(0)) {
      if ((Input.mousePosition - mouseWhenDown).magnitude < tapMouseMoveTolerance) {
        RaycastHit hitInfo = new RaycastHit();
        bool hit = Physics.Raycast(camera.ScreenPointToRay(Input.mousePosition), out hitInfo);
        if (hit) {
          Debug.Log("Hit " + hitInfo.transform.gameObject.name);
          Gooball goo = hitInfo.transform.gameObject.GetComponent<Gooball>();
          if (goo == null) {
            goo = hitInfo.transform.gameObject.GetComponentInParent<Gooball>();
          }
          if (goo != null) {
            Debug.Log("Hit goo " + goo.name);
            goo.Tap(hitInfo.point, strength);
          }
        }
      }
      strength = 0f;
    }
  }
}
